# deva

A converter between Devanagari and its transliterations. (c) 2018 - 2020 Sanskrit.de

## How to build

### Linux & macOS

```
stack init

stack build

stack exec deva-ui 8080 static
```

The last step will run the deva converter as a local server listening on port 8080. 
[http://127.0.0.1:8080/](http://127.0.0.1:8080/)

The UI is build with Threepenny GUI, a Haskell functional reactive web server.

### Windows

On Windows there are issues to build the regex-posix library. 

So after `stack init` edit the generated stack.yaml file and add the following lines:

```yaml
extra-deps:
  - regex-posix-clib-2.7

flags:
  regex-posix:
    _regex-posix-clib: true
```

then proceed normally with
```
stack build

stack exec deva-ui 8080 static
```


## how to build the electron UI

electron integration for the threepenny UI is done according to:
https://github.com/HeinrichApfelmus/threepenny-gui/blob/master/doc/electron.md

```bash
npm install

stack install --local-bin-path build
```

Now run your app with Electron: 

```bash
./node_modules/.bin/electron electron.js
```

## how to package the electron UI

before packaging make sure that `binArgs` is set correctly as:

```javascript
const binArgs = ['./resources/app/static'];
```

then run the following steps:

```bash
npm install electron-packager

./node_modules/.bin/electron-packager .

./node_modules/.bin/electron-packager . --ignore=app --ignore=src --ignore=server --ignore=ui --ignore=test --ignore=web
```

## The complete list of token mappings

|Harvard-Kyoto|Devanagari|IAST|ISO15919|
|----|----|----|----|
|a|अ|a|a|
|A|आ|ā|ā|
|i|इ|i|i|
|I|ई|ī|ī|
|u|उ|u|u|
|U|ऊ|ū|ū|
|R|ऋ|ṛ|r̥|
|RR|ॠ|ṝ|r̥̄|
|lR|ऌ|ḷ|l̥|
|lRR|ॡ|ḹ|l̥̄|
|e|ए|e|ē|
|o|ओ|o|ō|
|ai|ऐ|ai|ai|
|au|औ|au|au|
|k|क्|k|k|
|c|च्|c|c|
|T|ट्|ṭ|ṭ|
|t|त्|t|t|
|p|प्|p|p|
|g|ग्|g|g|
|j|ज्|j|j|
|D|ड्|ḍ|ḍ|
|d|द्|d|d|
|b|ब्|b|b|
|G|ङ्|ṅ|ṅ|
|J|ञ्|ñ|ñ|
|N|ण्|ṇ|ṇ|
|n|न्|n|n|
|m|म्|m|m|
|h|ह्|h|h|
|y|य्|y|y|
|r|र्|r|r|
|l|ल्|l|l|
|v|व्|v|v|
|Z|ज़्|z|z|
|f|फ़्|f|f|
|z|श्|ś|ś|
|S|ष्|ṣ|ṣ|
|s|स्|s|s|
|kh|ख्|kh|kh|
|ch|छ्|ch|ch|
|Th|ठ्|ṭh|ṭh|
|th|थ्|th|th|
|ph|फ्|ph|ph|
|gh|घ्|gh|gh|
|jh|झ्|jh|jh|
|Dh|ढ्|ḍh|ḍh|
|dh|ध्|dh|dh|
|bh|भ्|bh|bh|
|ġ|ग़्|ġ|ġ|
|0|०|0|0|
|1|१|1|1|
|2|२|2|2|
|3|३|3|3|
|4|४|4|4|
|5|५|5|5|
|6|६|6|6|
|7|७|7|7|
|8|८|8|8|
|9|९|9|9|
|M|ं|ṃ|ṁ|
|MM|ँ|m̐|m̐|
|H|ः|ḥ|ḥ|
|'|ऽ|'|'|
||्|||
|OM|ॐ|oṃ|ōṁ|
|\||।|।|।|
|\|\||॥|॥|॥|
|_|‌|_|_|
|\\ |‍ | \\ | \\ |
