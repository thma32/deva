module ArrayBench where

--import Generators
--import Tokenizer
--import           IoUtils                  (readFileUtf8)


--import           Data.List.Split
import           Data.Map         as Map hiding (filter, map, foldr, (!), )
import           Data.Map.Strict  as StrictMap hiding (filter, map, foldr, (!), )
import           GHC.Generics hiding (K1)
import           Data.Text hiding (splitOn, group, map, head, length)
import           DevanagariTokens
import           TokenTables
import           Data.Tuple

import Data.Array.IArray
import Data.Ix
import Data.Maybe

import Criterion.Main

data Key = K1 | K2 | K3 | K4 | K5 | K6 | K7 | K8 | K9 | K10 | K11 | K12 | K13 | K14 | K15 | K16 | K17 | K18 | K19 | K20 
         | K21 | K22 | K23 | K24 | K25 | K26 | K27 | K28 | K29 | K30 | K31 | K32 | K33 | K34 | K35 | K36 | K37 | K38 | K39 | K40 
         | K41 | K42 | K43 | K44 | K45 | K46 | K47 | K48 | K49 | K50 | K51 | K52 | K53 | K54 | K55 | K56 | K57 | K58 | K59 | K60 
         | K61 | K62 | K63 | K64 | K65 | K66 | K67 | K68 | K69 | K70 | K71 | K72 | K73 | K74 | K75 | K76 | K77 | K78 | K79 | K80 
         | K81 | K82 | K83 | K84 | K85 | K86 | K87 | K88 | K89 | K90 | K91 | K92 | K93 | K94 | K95 | K96 | K97 | K98 | K99 | K100
         deriving (Show, Read, Eq, Ord, Ix)

--array :: (Ix i) => (i,i) -> [(i,b)] -> Array i b
--squares =  array (1,100) [(i, i*i) | i <- [1..100]]

rawTable = [(K1, 1), (K2, 2), (K3, 3), (K4, 4), (K5, 5), (K6, 6), (K7, 7), (K8, 8), (K9, 9), (K10, 10), (K11, 11), (K12, 12), (K13, 13), (K14, 14), (K15, 15), (K16, 16), (K17, 17), (K18, 18), (K19, 19), (K20, 20), 
            (K21, 21), (K22, 22), (K23, 23), (K24, 24), (K25, 25), (K26, 26), (K27, 27), (K28, 28), (K29, 29), (K30, 30), (K31, 31), (K32, 32), (K33, 33), (K34, 34), (K35, 35), (K36, 36), (K37, 37), (K38, 38), (K39, 39), (K40, 40), 
            (K41, 41), (K42, 42), (K43, 43), (K44, 44), (K45, 45), (K46, 46), (K47, 47), (K48, 48), (K49, 49), (K50, 50), (K51, 51), (K52, 52), (K53, 53), (K54, 54), (K55, 55), (K56, 56), (K57, 57), (K58, 58), (K59, 59), (K60, 60), 
            (K61, 61), (K62, 62), (K63, 63), (K64, 64), (K65, 65), (K66, 66), (K67, 67), (K68, 68), (K69, 69), (K70, 70), (K71, 71), (K72, 72), (K73, 73), (K74, 74), (K75, 75), (K76, 76), (K77, 77), (K78, 78), (K79, 79), (K80, 80), 
            (K81, 81), (K82, 82), (K83, 83), (K84, 84), (K85, 85), (K86, 86), (K87, 87), (K88, 88), (K89, 89), (K90, 90), (K91, 91), (K92, 92), (K93, 93), (K94, 94), (K95, 95), (K96, 96), (K97, 97), (K98, 98), (K99, 99), (K100, 100)]
            
            
allKeys = [K1 , K2 , K3 , K4 , K5 , K6 , K7 , K8 , K9 , K10 , K11 , K12 , K13 , K14 , K15 , K16 , K17 , K18 , K19 , K20 
                  , K21 , K22 , K23 , K24 , K25 , K26 , K27 , K28 , K29 , K30 , K31 , K32 , K33 , K34 , K35 , K36 , K37 , K38 , K39 , K40 
                  , K41 , K42 , K43 , K44 , K45 , K46 , K47 , K48 , K49 , K50 , K51 , K52 , K53 , K54 , K55 , K56 , K57 , K58 , K59 , K60 
                  , K61 , K62 , K63 , K64 , K65 , K66 , K67 , K68 , K69 , K70 , K71 , K72 , K73 , K74 , K75 , K76 , K77 , K78 , K79 , K80 
                  , K81 , K82 , K83 , K84 , K85 , K86 , K87 , K88 , K89 , K90 , K91 , K92 , K93 , K94 , K95 , K96 , K97 , K98 , K99 , K100  ]          

biglist = allKeys ++ allKeys ++ allKeys ++ allKeys ++ allKeys ++ allKeys ++ allKeys ++ allKeys ++ allKeys ++ allKeys ++ allKeys ++ allKeys ++ allKeys ++ allKeys ++ allKeys ++ allKeys ++ allKeys ++ allKeys ++ allKeys ++ allKeys ++ allKeys ++ allKeys ++ allKeys ++ allKeys ++ allKeys

lookupTable :: Array Key Integer
lookupTable = array (K1,K100) rawTable 

addAll :: (Array Key Integer) -> Integer
addAll table = sum (map (table !) biglist)

lookupMap :: Map.Map Key Integer
lookupMap = Map.fromList rawTable

addAllFromMap :: (Map.Map Key Integer) -> Integer
addAllFromMap m = sum (map (\k -> fromMaybe 0 (Map.lookup k m)) biglist)

lookupStrictMap :: StrictMap.Map Key Integer
lookupStrictMap = StrictMap.fromList rawTable

addAllFromStrictMap :: (StrictMap.Map Key Integer) -> Integer
addAllFromStrictMap m = sum (map (\k -> fromMaybe 0 (StrictMap.lookup k m)) biglist)



arrayBench = do
   defaultMain [
         bench "lookup from Array"       (nf addAll              lookupTable )
       , bench "lookup from Map"         (nf addAllFromMap       lookupMap )
       , bench "lookup from StrictMap"   (nf addAllFromStrictMap lookupStrictMap )
       ]
   return ()

