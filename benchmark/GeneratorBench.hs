module GeneratorBench where

import Generators
import Tokenizer
import           IoUtils                  (readFileUtf8)

import           Control.Arrow    ((&&&))
import           Control.Category ((>>>))
import           Data.Char        (toLower)
import           Data.List        (group, sort)
import           Data.List.Split
import           Data.Map         as Map hiding (filter, map, foldr)
import           Data.Map         (Map)
import           Data.Text.Short 

import Criterion.Main

generatorBench = do
  iast <- readFileUtf8 "test/gita-complete.iast.txt"
  let normalizedText = fromIast iast
  defaultMain [
        bench "generate iast" $ nf toIast normalizedText
      , bench "generate deva" $ nf toDevanagari normalizedText
      ]
  return ()
