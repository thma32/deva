module Main where

import GeneratorBench (generatorBench)
import TokenizerBench (tokenizerBench)
import ArrayBench     (arrayBench)

main :: IO ()
main = do
-- arrayBench
  tokenizerBench
  generatorBench