module TokenizerBench where

import Criterion.Main
import Tokenizer
import IoUtils                  (readFileUtf8)
--import           Data.Text.Short 

tokenizerBench :: IO ()
tokenizerBench = do
  iast <- readFileUtf8 "test/gita-complete.iast.txt"
  defaultMain [
        bench "tokenize iast" $ nf fromIast iast

      ]
  return ()


