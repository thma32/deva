module Main where

import Graphics.UI.Gtk

main :: IO ()
main = do
  -- Initialize GTK
  initGUI

  -- Create a new window
  window <- windowNew
  set window [ windowTitle := "Input/Output Example" ]

  -- Create a vertical box to hold the input and output widgets
  vbox <- vBoxNew False 0
  containerAdd window vbox

  -- Create an input text box
  inputBox <- entryNew
  boxPackStart vbox inputBox PackNatural 0

  -- Create an output text box
  outputBox <- entryNew
  set outputBox [ entryEditable := False ] -- Make the output box read-only
  boxPackStart vbox outputBox PackNatural 0

  -- Set up a callback to update the output box when the input is changed
  on inputBox entryActivate $ do
    inputText <- get inputBox entryText
    set outputBox [ entryText := inputText ]

  -- Show the window and start the GTK main loop
  widgetShowAll window
  mainGUI