--module Spec where

import           Test.Hspec
--import           Test.QuickCheck hiding (forAll, Gen)
import           Tokenizer (fromIast, fromHarvard, fromDevanagari, fromIso, selectTokenizerByContent, Tokenizer)
import           Generators          (toIast, toHarvard, toDevanagari, toIso)
--import           System.IO
import           IoUtils                  (readFileUtf8)
import           Data.Text.Short (ShortText, pack)
import           Data.Sequence
import TokenTables ()
import qualified Hedgehog.Gen as Gen
import Test.Hspec.Hedgehog (Gen, forAll, hedgehog, tripping)
import DevanagariTokens


toEither :: Tokenizer -> ShortText -> Either String (Seq DevanagariToken)
toEither f = Right . f

-- `main` is here so that this module can be run from GHCi on its own.  It is
--  not needed for automatic spec discovery.
main :: IO ()
main = hspec spec

zwnj :: [Char]
zwnj = ['\8204'] -- Zero Width Non Joiner
zwj :: [Char]
zwj  = ['\8205'] -- Zero Width Joiner

spec :: Spec
spec = do
  describe "fromHarvard" $
    it "parses Harvard-Kyoto, IAST, ISO15919 and Devanagari to the same set of tokens" $ do
      harvard <- readFileUtf8 "test/smarami.hky.txt"
      iast   <- readFileUtf8 "test/smarami.ias.txt"
      deva   <- readFileUtf8 "test/smarami.dev.txt"
      iso    <- readFileUtf8 "test/smarami.iso.txt"
      fromHarvard harvard `shouldBe` fromIast iast
      fromHarvard harvard `shouldBe` fromDevanagari deva
      fromIso iso `shouldBe` fromDevanagari deva
  describe "toDevanagari" $
    it "generates Devanagari from parsed tokens" $ do
      harvard <- readFileUtf8 "test/smarami.hky.txt"
      iast   <- readFileUtf8 "test/smarami.ias.txt"
      deva   <- readFileUtf8 "test/smarami.dev.txt"
      iso    <- readFileUtf8 "test/smarami.iso.txt"
      toDevanagari (fromHarvard harvard) `shouldBe` deva
      toDevanagari (fromIast iast) `shouldBe` deva
      toDevanagari (fromDevanagari deva) `shouldBe` deva
      toDevanagari (fromIso iso) `shouldBe` deva
  describe "toHarvard" $ 
    it "generates Harvard-Kyoto from parsed tokens" $ do
      harvard <- readFileUtf8 "test/smarami.hky.txt"
      iast   <- readFileUtf8 "test/smarami.ias.txt"
      deva   <- readFileUtf8 "test/smarami.dev.txt"
      iso    <- readFileUtf8 "test/smarami.iso.txt"
      toHarvard (fromHarvard harvard) `shouldBe` harvard
      toHarvard (fromIast iast) `shouldBe` harvard
      toHarvard (fromDevanagari deva) `shouldBe` harvard
      toHarvard (fromIso iso) `shouldBe` harvard
  describe "toIast" $
    it "generates IAST from parsed tokens" $ do
      harvard <- readFileUtf8 "test/smarami.hky.txt"
      iast   <- readFileUtf8 "test/smarami.ias.txt"
      deva   <- readFileUtf8 "test/smarami.dev.txt"
      iso    <- readFileUtf8 "test/smarami.iso.txt"
      toIast (fromHarvard harvard) `shouldBe` iast
      toIast (fromIast iast) `shouldBe` iast
      toIast (fromDevanagari deva) `shouldBe` iast
      toIast (fromIso iso) `shouldBe` iast
  describe "toIso" $
    it "generates Iso15919 from parsed tokens" $ do
      harvard <- readFileUtf8 "test/smarami.hky.txt"
      iast   <- readFileUtf8 "test/smarami.ias.txt"
      deva   <- readFileUtf8 "test/smarami.dev.txt"
      iso    <- readFileUtf8 "test/smarami.iso.txt"
      toIso (fromHarvard harvard) `shouldBe` iso
      toIso (fromIast iast) `shouldBe` iso
      toIso (fromDevanagari deva) `shouldBe` iso
      toIso (fromIso iso) `shouldBe` iso
  describe "roundtrip" $
    it "has output identical to input after complex roundtrip" $ do
      iast   <- readFileUtf8 "test/gita.ias.txt"
      let roundtrip = (toIast . fromIso . toIso . fromDevanagari . toDevanagari . fromHarvard . toHarvard .fromIast) iast
      roundtrip `shouldBe` iast
  describe "tokenBased roundtrip" $ do
    it "works for all vowels" $ hedgehog $ do
      x <- forAll (Gen.enumBounded :: Gen Vowel)
      tripping (fromList [Vow x]) toDevanagari (toEither fromDevanagari)
      tripping (fromList [Vow x]) toHarvard (toEither fromHarvard)
      tripping (fromList [Vow x]) toIast (toEither fromIast)
      tripping (fromList [Vow x]) toIso (toEither fromIso)
    it "works for all consonants" $ hedgehog $ do
      x <- forAll (Gen.enumBounded :: Gen Consonant)
      tripping (fromList [Cons x, Vow A]) toDevanagari (toEither fromDevanagari)
      tripping (fromList [Cons x, Vow A]) toHarvard (toEither fromHarvard)
      tripping (fromList [Cons x, Vow A]) toIast (toEither fromIast)
      tripping (fromList [Cons x, Vow A]) toIso (toEither fromIso)
    it "works for all digits" $ hedgehog $ do
      x <- forAll (Gen.enumBounded :: Gen Digit)
      tripping (fromList [Dig x]) toDevanagari (toEither fromDevanagari)
      tripping (fromList [Dig x]) toHarvard (toEither fromHarvard)
      tripping (fromList [Dig x]) toIast (toEither fromIast)
      tripping (fromList [Dig x]) toIso (toEither fromIso)
    it "works for special characters" $ hedgehog $ do
      let specials = [Anusvara,Anunasika,Visarga,Avagraha,Virama,OM,PurnaViram,DeerghViram]
      x <- forAll (Gen.element specials)
      tripping (fromList [x]) toDevanagari (toEither fromDevanagari)

  describe "selectParserByContent" $
    it "selects correct parse function based on input" $ do
      harvard <- readFileUtf8 "test/smarami.hky.txt"
      iast   <- readFileUtf8 "test/smarami.ias.txt"
      deva   <- readFileUtf8 "test/smarami.dev.txt"
      iso    <- readFileUtf8 "test/smarami.iso.txt"
      selectTokenizerByContent deva   deva   `shouldBe` fromDevanagari deva
      selectTokenizerByContent iast   iast   `shouldBe` fromIast iast
      selectTokenizerByContent harvard harvard `shouldBe` fromHarvard harvard
      selectTokenizerByContent iso    iso    `shouldBe` fromIso iso
  describe "ligature handling" $ do
      it "produces correct consonant ligatures " $ do
        let harvard = pack "sadgamaye"
        (toDevanagari . fromHarvard) harvard `shouldBe` pack "सद्गमये"
        let harvard = pack "vakSyAmi"
        (toDevanagari . fromHarvard) harvard `shouldBe` pack "वक्ष्यामि"
      it "can enforce usage of an explicit virama to suppress consonant ligatures " $ do
        let harvard = pack "sad_gamaye"
            deva   = pack ("सद्" ++ zwnj ++ "गमये")
        (toDevanagari . fromHarvard) harvard `shouldBe` deva
      it "can enforce usage of regular consonant ligatures instead of complex ligatures" $ do
        let harvard = pack "vak\\SyAmi"
            deva   = pack ("वक्" ++ zwj ++ "ष्यामि") 
        (toDevanagari . fromHarvard) harvard `shouldBe` deva
  describe "reading of special chars for viramas" $ do
      it "reads | correctly as PurnaViram" $ do
        let harvard = pack "OM zAntiH |"
            deva   = pack "ॐ शान्तिः ।"
        (toDevanagari . fromHarvard) harvard `shouldBe` deva
      it "reads || correctly as DeerghViram" $ do
        let harvard = pack "OM zAntiH ||"
            deva   = pack "ॐ शान्तिः ॥"
        (toDevanagari . fromHarvard) harvard `shouldBe` deva