//var serverUrl = "http://35.234.95.224:3000"; // PROD
var serverUrl = "http://localhost:3000"; // TEST

function getInputAsJson() {
    return "\"" + $('#input').val() + " \"";
}

function firstToUpper(word) {
    return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
}

function convertBox(boxName, status) {
    $.ajax({
      type:    'POST',
      url:     serverUrl + "/convertTo" + firstToUpper(boxName),
      data:    getInputAsJson(),
      success: function(data){
                    $("#output-" + boxName).html(data);
               },
      error:   function(xhr, textStatus, error){
                    status.error = true;
               }
    });


}

$(document).ready(function() {
    new ClipboardJS('.clp');

    $("#driver").click(function(event){
        let status = {error:false};
        $('#alert').css('opacity', '0.0');

        convertBox("deva", status)
        convertBox("iast", status)
        convertBox("hk", status)
        convertBox("iso", status)

        if (status.error) {
            $("#alert").css('opacity', '1.0');
        }

    });
});