module Main where

import           IoUtils              (readFileUtf8, writeFileUtf8)
import           System.IO            (hSetEncoding, stdin, stdout, utf8)
import           Generators           (toDevanagari, toHarvard, toIast, toIso)
import           Tokenizer            (selectTokenizerByContent)
import           System.Directory     (doesFileExist)
import           System.Environment   (getArgs)


main :: IO ()
main = do
  putStrLn "deva v1.0: A converter between Devanagari and its transliterations. (c) 2018 - 2020 Sanskrit.de\n"
  hSetEncoding stdin  utf8
  hSetEncoding stdout utf8
  args <- getArgs

  if Prelude.null args
    then putStrLn "Error: please specify an input filename"
    else do
      let inFile = Prelude.head args
      fileFound <- doesFileExist inFile
      if fileFound
        then do
          inputString <- readFileUtf8 inFile
          let tokenizer = selectTokenizerByContent inputString
          let tokens = tokenizer inputString
          writeFileUtf8 (inFile ++ ".dev.txt") $ toDevanagari tokens
          putStrLn $ "Devanagari file " ++ (inFile ++ ".dev.txt") ++ " written"
          writeFileUtf8 (inFile ++ ".hky.txt") $ toHarvard tokens
          putStrLn $ "Harvard-Kyoto file " ++ (inFile ++ ".hky.txt") ++ " written"
          writeFileUtf8 (inFile ++ ".ias.txt") $ toIast tokens
          putStrLn $ "IAST file " ++ (inFile ++ ".ias.txt") ++ " written"
          writeFileUtf8 (inFile ++ ".iso.txt") $ toIso tokens
          putStrLn $ "ISO15919 file " ++ (inFile ++ ".iso.txt") ++ " written"
        else putStrLn $ "Error: the file " ++ inFile ++ " could not be found."


