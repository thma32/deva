{-# LANGUAGE CPP #-}
module Paths (getStaticDir) where

--import Control.Monad
--import System.FilePath

#if defined(CABAL)
-- using cabal
import qualified Paths_deva (getDataDir)
import Control.Monad

getStaticDir :: IO FilePath
getStaticDir = (</> "static") `liftM` Paths_deva.getDataDir

#else
-- using GHCi

getStaticDir :: IO FilePath
getStaticDir = return "static"

#endif

-- | Sanskrit.de homepage.
--sanskritUrl :: String
--sanskritUrl = "http://sanskrit.de"
