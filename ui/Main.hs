module Main where

import System.Environment (getArgs)
import System.IO
import Ui            (start, up)


portAndDir []         = ["8023", "static"]
portAndDir [port]     = [port,"static"]
portAndDir [port,dir] = [port,dir]

main :: IO ()
main = do
    hSetBuffering stdout LineBuffering
    args <- getArgs
    let [port, staticDir] = portAndDir args
    --[port, staticDir] <- getArgs
    start (read port) staticDir
    
