{-# LANGUAGE QuasiQuotes       #-}
module Ui where

import qualified Graphics.UI.Threepenny as UI
import           Graphics.UI.Threepenny.Core

import           System.Process           (createProcess, shell)
import           System.Info              (os)

import           DevanagariTokens
import           Tokenizer                (tokenize)
import           Generators               (toIso, toHarvard, toIast, toDevanagari, tokenMapToHtml)
import           Data.Text.Short          (ShortText, pack, unpack)
import           Data.Sequence.Internal   (Seq)
import           Control.Monad            (void)
import           Text.RawString.QQ        (r)

-- | main entry point for electron based deployment.
start :: Int -> FilePath -> IO ()
start port static =
  startGUI defaultConfig
    { jsPort = Just port
    , jsCustomHTML     = Just "deva.html"
    , jsStatic         = Just static
    , jsCallBufferMode = BufferRun
    } setup

-- | for local testing outside of electron
up :: IO ()
up = do
  launchSiteInBrowser
  start 8023 "static"

-- | define some elements frequently used in bootstrap
section = mkElement "section"
i       = mkElement "i" 
script  = mkElement "script"

initialText = "OM zAntiH"

setup :: Window -> UI ()
setup window = void $ do
  inputField  <- UI.textarea #. "form-control"
                             # set text initialText
                             # set (attr "rows") "5"
                             # set (attr "id") "input"
  outputDeva  <- UI.pre      # set style [("color", "black"), ("font-size", "12pt")]
                             # set (attr "id") "output-deva"
                             # set text "---"
  outputIAST  <- UI.pre      # set style [("color", "black"), ("font-size", "12pt")]
                             # set (attr "id") "output-iast"
                             # set text "---"
  outputHK    <- UI.pre      # set style [("color", "black"), ("font-size", "12pt")]
                             # set (attr "id") "output-hk"
                             # set text "---"
  outputISO   <- UI.pre      # set style [("color", "black"), ("font-size", "12pt")]
                             # set (attr "id") "output-iso"
                             # set text "---"

  setWindowLayout window inputField outputDeva outputIAST outputHK outputISO

  textIn <- stepper initialText $ UI.valueChange inputField
  let
      str      = fmap pack textIn  :: Behavior ShortText
      tokens   = fmap tokenize str :: Behavior (Seq DevanagariToken)
      textDeva = unpack . toDevanagari <$> tokens
      textIast = unpack . toIast       <$> tokens
      textHK   = unpack . toHarvard    <$> tokens
      textISO  = unpack . toIso        <$> tokens

  element outputDeva # sink text textDeva
  element outputIAST # sink text textIast
  element outputHK   # sink text textHK
  element outputISO  # sink text textISO


setWindowLayout window inputField outputDeva outputIAST outputHK outputISO =
    getBody window #+ [

      UI.div # set UI.id_ "wrapper" #+ [
        section # set UI.id_ "inner-headline" #+ [
          UI.div #. "container" #+ [
            UI.div #. "row" #+ [
              UI.div #. "span12" #+ [
                UI.div #. "inner-heading" #+ [
                  UI.h2 #+ [string "The Devanagari Converter"]
                , UI.h5 # set style [("color", "white")]
                        #+ [ string "From transliterations to Devanagari and back."]
                ]
              ]
            ]
          ]
        ],
        section # set UI.id_ "content" #+ [
          UI.div #. "container" #+ [
            UI.div #. "row" #+ [
              UI.div #. "span12" # set style [("color", "black")] #+ [
                UI.h5 #+ [
                  string "This tool provides conversion from Latin transliterations to Devanagari and back. ",
                  UI.br,
                  string "You can input text in Harvard-Kyoto, IAST, ISO15919 and Devanagari Unicode. "
                ],
                UI.p #+ [
                  string "An example: to display the word 'Shanti' in Devanagari just ",
                  string "enter 'zAnti' or 'śānti'. As a result the text will be displayed in Devanagari: 'शान्ति'. ",
                  string "You can as well enter a text in Devanagari and it will be displayed in the three transliterations. ",
                  string "The text in the output areas is updated while you type."
                ],
                UI.div #. "contactForm" #+ [
                    element inputField
                ]
              ]
            ],
            UI.div #. "row" #+ [
              UI.div #. "span6" #+ [
                UI.h6 #+ [string "Devanagari"],
                UI.p #+ [string "The original Sanskrit script. As an introduction we recommend the title ",
                         UI.anchor
                          # set (attr "target") "new"
                          # set UI.href "http://sanskrit.de/raja-verlag.html#sanskrit-devanagari-1" #+ [string "Sanskrit Devanagari"],
                         string " (in German)."
                ],
                element outputDeva,
                UI.button #. "clp btn btn-small btn-theme btn-rounded floatright"
                          # set (attr "data-clipboard-target") "#output-deva" #+ [
                  i #. "icon-copy",
                  string "copy text to clipboard"
                ]
              ],
              UI.div #. "span6" #+ [
                UI.h6 #+ [string "IAST"],
                UI.p #+ [string "The International Alphabet of Sanskrit Transliteration. Widely used in Indology and Yoga literature."],
                element outputIAST,
                UI.button #. "clp btn btn-small btn-theme btn-rounded floatright"
                          # set (attr "data-clipboard-target") "#output-iast" #+ [
                  i #. "icon-copy",
                  string "copy text to clipboard"
                ]
              ]
            ],

            UI.div #. "row" #+ [
              UI.div #. "span6" #+ [
                UI.h6 #+ [UI.anchor # set UI.href "#box-harvard" #+ [string "Harvard-Kyoto"]],
                UI.p #+ [string "the most easy way to type sanskrit texts with your computer. See tutorial below."],
                element outputHK,
                UI.button #. "clp btn btn-small btn-theme btn-rounded floatright"
                          # set (attr "data-clipboard-target") "#output-hk" #+ [
                  i #. "icon-copy",
                  string "copy text to clipboard"
                ]
              ],
              UI.div #. "span6" #+ [
                UI.h6 #+ [string "ISO15919"],
                UI.p #+ [string "An extended version of IAST that supports other Indian languages besides Sanskrit"],
                element outputISO,
                UI.button #. "clp btn btn-small btn-theme btn-rounded floatright"
                          # set (attr "data-clipboard-target") "#output-iso" #+ [
                  i #. "icon-copy",
                  string "copy text to clipboard"
                ]
              ]
            ],

            UI.div #. "row" # set style [("color", "black")] #+ [
            ],
            UI.div #. "row" # set style [("color", "black")] #+ [
            ],
            
            UI.div #. "row" # set style [("color", "black")] #+ [
              UI.div #. "span6" #+ [
                UI.p # set html hkTutorial
              ],
              UI.div #. "span6" #+ [
                UI.p # set html tokenMap
              ]
            ]
            
          ]

        ]
      ]
     ]


hkTutorial :: String
hkTutorial = [r|
         <h4 id=\"box-harvard\">Harvard Kyoto transliteration – a brief tutorial</h4>
         <p>
             Harvard Kyoto-transliteration allows to type Sanskrit words with a normal computer keyboard. 
             It is quite easy to learn, just remember the following rules:
         </p>
         <p>
             <ul>
             <li>
                 Long vowels (marked by a dash over them in IAST) are always typed in upper case:<br>
                 <table class="table table-striped">
                    <thead><tr><th width="50%">Harvard-Kyoto</th><th width="50%">IAST</th></tr></thead>
                    <tbody>
                      <tr><td>mahAbhArata</td>  <td>mahābhārata</td></tr>
                      <tr><td>bhagavadgItA</td> <td>bhagavadgītā</td></tr>
                    </tbody>
                 </table>  
             </li>
             <li>
                 Upper case letters are also used to type letters that have a dot below them in IAST:<br>
                  <table class="table table-striped">
                     <thead><tr><th width="50%">Harvard-Kyoto</th><th width="50%">IAST</th></tr></thead>
                     <tbody>
                       <tr><td>vATa</td>      <td>vāṭa</td></tr>
                       <tr><td>ISa</td>       <td>īṣa</td></tr>
                       <tr><td>ArUDha</td>    <td>ārūḍha</td></tr>
                       <tr><td>haMsa</td>     <td>haṃsa</td></tr>
                       <tr><td>bAlaH</td>     <td>bālaḥ</td></tr>
                       <tr><td>prANAyAma</td> <td>prāṇāyāma</td></tr>
                       <tr><td>RSi</td>       <td>ṛṣi</td></tr>
                       <tr><td>amRta</td>     <td>amṛta</td></tr>
                     </tbody>
                  </table>  

             </li>
             <li>
                 The consonant ś is special. As 's' and 'S' are already used for 's' and 'ṣ' the letter 'z' is used:
                 
                 <table class="table table-striped">
                    <thead><tr><th  width="50%">Harvard-Kyoto</th><th  width="50%">IAST</th></tr></thead>
                    <tbody>
                      <tr><td>zAstra</td>  <td>śāstra</td></tr>
                      <tr><td>zAnti</td>   <td>śānti</td></tr>
                    </tbody>
                 </table>                   

             </li>
             <li>
                 Please also note the characters ñ – J and ṅ - G:
                 
                 <table class="table table-striped">
                    <thead><tr><th width="50%">Harvard-Kyoto</th><th width="50%">IAST</th></tr></thead>
                    <tbody>
                      <tr><td>pataJjali</td>  <td>patañjali</td></tr>
                      <tr><td>aGga</td>       <td>aṅga</td></tr>
                    </tbody>
                 </table>                    

             </li>
             <li>
                 Only for experts:<br>
                 standard Harvard-Kyoto does not support the anunasika (m̐ in IAST). With our converter you enter it as MM:<br>
                 <table class="table table-striped">
                    <thead><tr><th width="50%">Harvard-Kyoto</th><th width="50%">IAST</th></tr></thead>
                    <tbody>
                      <tr><td>tAMMllokAn</td>  <td>tām̐llokān</td></tr>
                    </tbody>
                 </table>  

             </li>
               <li>
                   Special handling to avoid consonant ligatures (also not part of standard transliterations):<br>
                   
                 <table class="table table-striped">
                    <thead><tr><th width="50%">Harvard-Kyoto</th><th width="50%">Devanagari</th></tr></thead>
                    <tbody>
                      <tr><td>sadgamaye</td>   <td>सद्गमये</td></tr>
                      <tr><td>sad_gamaye</td>  <td>सद्‌गमये</td></tr>
                      <tr><td>vakSyAmi</td>    <td>वक्ष्यामि</td></tr>
                      <tr><td>vak\\SyAmi</td>  <td>वक्‍‍ष्यामि</td></tr>
                    </tbody>
                 </table>                     

               </li>
             </ul> 
 
 |]

tokenMap :: String
tokenMap = [r|<h4>The complete table of character mappings</h4>
<table class="table table-striped">
  <thead>
    <tr>
      <th>Harvard-Kyoto</th>
      <th>Devanagari</th>
      <th>IAST</th>
      <th>ISO15919</th>
    </tr>
  </thead>
<tbody>
|] ++ unpack tokenMapToHtml ++ "</tbody></table>"


-- | convenience function that opens the 3penny UI in the default web browser
launchSiteInBrowser:: IO ()
launchSiteInBrowser = do 
  _  <- createProcess  (shell $ openCommand ++ url) 
  pure () 
    where
      url = "http://localhost:8023"
      openCommand = case os of
        "darwin"  -> "open "
        "linux"   -> "xdg-open "
        "mingw32" -> "start "
        _         -> "open "

